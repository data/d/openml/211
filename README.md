# OpenML dataset: longley

https://www.openml.org/d/211

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

Data from StatLib (ftp stat.cmu.edu/datasets)

 The infamous Longley data, "An appraisal of least-squares programs from
  the point of view of the user", JASA, 62(1967) p819-841.

 Variables are: Number of people employed   (usually the y variable)
                GNP implicit price deflator
                GNP
                Unemployed
                Armed forces
                Non-institutionalized population >=14 years of age
                Year

 Employment is being treated as the class
 attribute.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/211) of an [OpenML dataset](https://www.openml.org/d/211). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/211/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/211/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/211/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

